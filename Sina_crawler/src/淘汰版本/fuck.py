# -*- coding: utf-8 -*-

from login import *
from weiboID import weiboID
from weiboID import myID
from cookies import *
import io
import re
import requests
import json
import os
from bs4 import BeautifulSoup
import lxml
#初始化文件路径
try:
    os.chdir('page')
    os.chdir('..')
except:
    os.mkdir('page')
getcookies()
for i in range(0,len(weiboID)):
	#依次访问主页链接
    url='http://weibo.com/u/'+weiboID[i]
	#获得cookie池列表长度
    t = random.randint(0,len(cookiespool))-1
	#从cookie池中随机提取一个cookie进行访问
    data = requests.get(url,cookies=cookiespool[t]).text
	#解析主页
    soup= BeautifulSoup(data,'lxml')
    os.chdir('page')
    fout=open('%s.txt'%i,'w+',encoding='utf-8')
	#规范写入格式
    fout.write(soup.prettify())
    fout.close()
    os.chdir('..')
