import time
import re
import os
from collections import deque  #队列
from multiprocessing import Process, Lock
#from driver import Driver
from crawler_selenium import Crawler
#from login_selenium import Login
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.action_chains import ActionChains

from weiboID import weiboID
from weiboID import myID

#driver2=webdriver.Chrome()
#driver3=webdriver.Chrome()
userID_queue = deque()

#for i in range(0,len(weiboID)):
#   userID_queue.append(weiboID[i])


class mCrawler(Process, Crawler):
    def __init__(self, loop, lock):
        Process.__init__(self)
        self.loop = loop
        self.lock = lock

    def run(self):
        for count in range(self.loop):
            time.sleep(0.2)
            self.lock.acquire()
            username = myID[count][0]
            password = myID[count][1]
            print(count)
            Crawler.login(username, password)
            self.lock.release()


if __name__ == '__main__':
    lock = Lock()
    for i in range(0, 2):
        p = mCrawler(i, lock)
        p.run()