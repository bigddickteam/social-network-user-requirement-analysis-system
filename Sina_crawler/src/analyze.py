# 文本处理
from numpy import *
from pylab import plot, show, subplot, title
#from sklearn import cross_validation
import numpy as np
from pylab import plot, show
from cvxopt import solvers, matrix
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
from itertools import product
import pydotplus
import os
import io
from sql import MSSQL
import jieba
import re
from weiboID import WordSet

class Analyze():
    # analyse.set_stop_words("stop_words.txt")

    city1=[
        '北京','上海','广州','深圳','成都','杭州','武汉','重庆','南京','天津','苏州','西安','长沙','沈阳','青岛','郑州','大连','东莞','宁波'
    ]
    city2=[
        '厦门','福州','无锡','合肥','昆明','哈尔滨','济南','佛山','长春','温州','石家庄','南宁','常州','泉州','南昌','贵阳','太原','烟台','嘉兴','南通','金华','珠海','惠州','徐州','海口','乌鲁木齐','绍兴','中山','台州','兰州市','潍坊','保定','镇江','扬州','桂林','唐山','三亚','湖州','呼和浩特','廊坊','洛阳','威海','盐城','临沂','江门','汕头','泰州','漳州','邯郸','济宁','芜湖','淄博','银川','柳州','绵阳','湛江','鞍山','赣州','大庆','宜昌','包头','咸阳','秦皇岛','株洲','莆田','吉林','淮安','肇庆','宁德','衡阳','南平','连云港','丹东','丽江','揭阳','延边朝鲜族自治州、舟山','九江','龙岩','沧州','抚顺','襄阳','上饶','营口','三明','蚌埠','丽水','岳阳','清远','荆州','泰安','衢州','盘锦','东营','南阳','马鞍山','南充','西宁','孝感','齐齐哈尔'
    ]

    def disdata(self):
        # 数据的离散化保存
        mssql = MSSQL()
        oids = mssql.dataoidget()
        fout = open('data.txt','w+',encoding='utf-8')
        for oid in oids:
            print(oid['Oid'])
            datasp=mssql.orgdataget(oid['Oid'].rstrip())
            datas=datasp[0]
            keywordsp=mssql.keywordget(oid['Oid'].rstrip())
            keywords=keywordsp
            if datas['Gender'].rstrip()=='女':
                fout.write('1,')
            else:
                fout.write('-1,')

            locals=jieba.cut(datas['Location'])
            for local in locals:
                if local in self.city1:
                    fout.write('1,')
                    break
                elif local in self.city2:
                    fout.write('2,')
                    break
                else:
                    fout.write('3,')
                    break

            try:
                year = re.findall(r'(.*)年',datas['Birthday'])
                age=2017-int(year[0])
            except:
                age=25
            finally:
                fout.write('%s,'%age)
            
            try:
                tags=jieba.cut(datas['Tag'])
                cnt=0
                for tag in tags:
                    if tag in WordSet:
                        cnt+=1
                fout.write('%s,'%cnt)
            except:
                fout.write('0,')

            try:
                edus=jieba.cut(datas['EducationBackground'])
                cnt=0
                for edu in edus:
                    if edu=='大学':
                        cnt+=1
                fout.write('%s,'%cnt)
            except:
                fout.write('0,')
            
            knum=len(keywords)
            fout.write('%s,'%knum)

            fout.write('%s\n'%datas['select'])

        fout.close()        

    def LNP(self):
        # 读取数据集文件
        # os.chdir('..')

        data = genfromtxt('data.csv',
                        delimiter=',', usecols=(0, 1, 2, 3,4,5))
        target = genfromtxt('data.csv',
                            delimiter=',', usecols=(6))
        t = zeros(len(target))
        t[target == -1] = -1
        t[target == 1] = 1

        # os.chdir('LNP')
        # print(data.shape)
        # print(target)  # 确认读取数据集大小
        # 此处使用二次规划
        # 交叉验证，将数据的40%用来做测试集
        #train, test, t_train, t_test = cross_validation.train_test_split(data, t,test_size=0.4, random_state=0)
        #定义常量
        #取样率：选择部分带标签的数据
        perc = 50
        mod = int(100 / perc)
        #数据长度
        n = len(data)
        #K与α的选择
        K = 10
        alpha = 0.99

        TARGET = np.array(target)
        W = []
        DATA = np.array(data)

        #等间距选择带标签数据集
        for i in range(0, n):
            if i % mod != 0:
                TARGET[i] = 0

        # 二次规划求解W论文（7）试
        for i in range(0, n):
            # K近邻求解
            diffmat = np.tile(DATA[i], (n, 1)) - DATA
            sqrtdiffmat = np.multiply(diffmat, diffmat)
            sqrtdis = sqrtdiffmat.sum(axis=1)
            sortlist = sqrtdis.argsort()
            kneib = []
            for j in range(0, K):
                kneib.append(sortlist[j])

            # 构造二次规划矩阵
            P = matrix(np.zeros([K, K]))
            for j in range(0, K):
                for k in range(0, K):
                    P[j, k] = np.dot(DATA[i] - DATA[kneib[j]],
                                    DATA[i] - DATA[kneib[k]]) * 2
            #二次规划函数常量
            Q = matrix(np.zeros(K))
            G = matrix(-1 * np.identity(K))
            H = matrix(np.zeros(K))
            B = matrix(np.ones(1)).T
            A = matrix(np.ones(K)).T

            # 函数QP求解
            sol = solvers.qp(P, Q, G, H, A, B)
            w = np.zeros(n)
            for j in range(0, K):
                w[int(kneib[j])] = sol['x'][j]
            W.append(w)

        # 论文（15）试
        W = np.matrix(W)
        # TARGET=np.float(TARGET)
        F = (1 - alpha) * np.matrix(np.identity(n) - alpha * W).I * np.matrix(TARGET).T

        cnt = 0.0
        # 计算正确率
        for i in range(0, n):
            if F[i, 0] * target[i] > 0:
                cnt += 1
        print("标签占比：%s%%"%perc)
        print(float(cnt) / n)
        #图像输出
        subplot(211)
        title('Orgin_data')
        plot(data[target == -1, 0], data[target == -1, 1], 'bo')
        plot(data[target == 1, 0], data[target == 1, 1], 'ro')

        c = zeros(n)
        for i in range(0, n):
            if F[i, 0] < 0:
                c[i] = -1.0
            else:
                c[i] = 1.0
        #对比
        subplot(212)
        title('LNP_data')
        plot(data[c == -1, 0], data[c == -1, 1], 'bo')
        plot(data[c == 1, 0], data[c == 1, 1], 'ro')
        show()

    def ID3(self):
        data = np.genfromtxt('data.csv',
                    delimiter=',', usecols=(0, 1, 2, 3,4,5))
        target = np.genfromtxt('data.csv',
                            delimiter=',', usecols=(6))
        t = np.zeros(len(target))
        t[target == -1] = -1
        t[target == 1] = 1
        X = data[:, [0, 1]]
        y = t

        # 训练模型，限制树的最大深度4
        clf = DecisionTreeClassifier(max_depth=10)
        # 拟合模型i
        clf.fit(X, y)


        # 画图
        x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
        y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.1),
                            np.arange(y_min, y_max, 0.1))

        Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)

        plt.contourf(xx, yy, Z, alpha=0.3)  # 分类面
        plt.scatter(X[:, 0], X[:, 1], c=y)  # 点
        plt.show()

        # 决策树可视化
        dot_data = tree.export_graphviz(clf, out_file=None)
        graph = pydotplus.graph_from_dot_data(dot_data)
        graph.write_jpg('data_banknote_authentication.jpg')
        print(clf.score(X,y))

