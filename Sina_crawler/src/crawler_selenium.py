import time
import re
import os
from sql import MSSQL
from analyze import Analyze
import jieba.analyse as JA
from weiboID import WordSet

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.action_chains import ActionChains

from driver import Driver
from login_selenium import Login

JA.set_stop_words('stop_words.txt')
class Crawler(Login):
    driver = Login.driver
    
    def getuserinfo(self, user_id):
        try:
            err = 0
            # 依次访问主页链接
            url = 'http://weibo.com/p/100505' + user_id + '/info?mod=pedit_more'
            self.driver.set_page_load_timeout(15)
            # self.driver.get(url)
            try:
                self.driver.get(url)
                # ui.WebDriverWait(self.driver, 5).until(self.driver.find_element_by_xpath(
                #    "//span[@class='pt_title S_txt2']"))
            except:
                self.driver.refresh()
            finally:

                js = "var q=document .body .scrollTop =10000"
                self.driver.execute_script(js)
                # 用户ID
                print(user_id)
                # 用户昵称
                user_name = self.driver.find_element_by_xpath(
                    "//div[@class='pf_username']/h1")
                print(user_name.text)
                err += 1  # 1

                # 关注人数，粉丝数，微博数
                user_num = self.driver.find_elements_by_xpath(
                    "//table[@class='tb_counter']/tbody/tr/td/a")
                gz = user_num[0].text  # 关注数
                num_gz = re.findall(r'(\w*[0-9]+)\w*', gz)
                fs = user_num[1].text  # 粉丝数
                num_fs = re.findall(r'(\w*[0-9]+)\w*', fs)
                wb = user_num[2].text  # 微博数
                num_wb = re.findall(r'(\w*[0-9]+)\w*', wb)
                #print("关注:%s粉丝:%s微博:%s" % (num_gz, num_fs, num_wb))
                err += 1  # 2

                # 用户个人信息
                # 每个人公开的信息都不一样，除了输出的字符名称外没找到其它标识符
                user_infoname = self.driver.find_elements_by_xpath(
                    "//span[@class='pt_title S_txt2']")
                user_infovalue = self.driver.find_elements_by_xpath(
                    "//li[@class='li_1 clearfix']/span[2]")
                err += 1  # 3
                # 初始化允许null值的变量，防止错误输入。
                Location = Gender = SexualOrientation = EmotionalSituation = 'NULL'
                Birthday = EducationBackground = Tag = 'NULL'
                err += 1  # 4
                i = 0
                for j in range(0, len(user_infovalue)):
                    if user_infoname[i].text == "博客：":
                        i += 1
                    if user_infoname[i].text == "所在地：":
                        Location = user_infovalue[j].text
                    if user_infoname[i].text == "性别：":
                        Gender = user_infovalue[j].text
                    if user_infoname[i].text == "性取向：":
                        SexualOrientation = user_infovalue[j].text
                    if user_infoname[i].text == "感情状况：":
                        EmotionalSituation = user_infovalue[j].text
                    if user_infoname[i].text == "邮箱：":
                        eMail = user_infovalue[j].text
                    if user_infoname[i].text == "生日：":
                        Birthday = user_infovalue[j].text
                    if user_infoname[i].text == "大学：":
                        EducationBackground = user_infovalue[j].text
                    if user_infoname[i].text == "标签：":
                        Tag = user_infovalue[j].text
                    i += 1
                err += 1  # 5

                '''
                print('%s %s' %
                    (user_infoname[i].text, user_infovalue[i].text))
                '''
                try:
                    Sqlin_Ouser = MSSQL()
                    Sqlin_Ouser.iOuser(user_id, user_name.text, Gender, Location, Birthday,
                                       SexualOrientation, Tag, EducationBackground,
                                       EmotionalSituation, num_wb[0], num_gz[0], num_fs[0])
                    err += 1  # 6
                except:
                    print("sql error001")

        except:
            print("ERROR%s:Crawler.getuserinfo had trouble" % err)
            try:
                if err == 1:
                    # 删除企业ID
                    Sql = MSSQL()
                    Sql.Delid(user_id)
                if err==0:
                    time.sleep(1)
                    Sql = MSSQL()
                    Sql.Delid(user_id)
            except:
                pass

    def getuserfollowers(self, user_id):
        try:

            # 访问粉丝列表
            url = 'http://weibo.com/p/100505' + user_id + \
                '/follow?relate=fans&from=100505&wvr=6&mod=headfans&current=fans#place'
            self.driver.set_page_load_timeout(10)
            # self.driver.get(url)
            try:
                self.driver.get(url)
                # ui.WebDriverWait(self.driver, 5).until(self.driver.find_element_by_xpath(
                #    "//span[@class='pt_title S_txt2']"))
            except:
                self.driver.refresh()
            finally:
                # sleep这么多次都是为了防403啊
                time.sleep(2)
                # 当前用户ID
                print('当前用户%s' % user_id)

                # 新浪限制只能看前五页的粉丝和关注
                for i in range(2, 5):
                    try:

                        # 当前页面粉丝数量以及改粉丝的粉丝数量和微博数量（判断新浪僵尸粉)
                        user_followers = self.driver.find_elements_by_xpath(
                            "//ul[@class='follow_list']/li/dl/dd/div[1]/a[1]")
                        # user_followersid=self.driver.
                        follower_followers = self.driver.find_elements_by_xpath(
                            "//div[@class='info_connect']/span[2]/em")
                        follower_weibos = self.driver.find_elements_by_xpath(
                            "//div[@class='info_connect']/span[3]/em")
                        for p in range(0, len(user_followers)):
                            # 判断用户的粉丝是否大于50，发微博量是否大于十条
                            if(int(follower_followers[p].text) > 50 and int(follower_weibos[p].text) > 10):
                                # 输出该粉丝ID
                                print(user_followers[p].text)
                                # 通过正则找到该粉丝usercard中的ID
                                # 该粉丝个人主页
                                #follower = 'https://www.weibo.com'+user_followers[i].get_attribute("usercard")
                                user_follower_id = re.findall(
                                    r'id=(.*)&refer', user_followers[p].get_attribute("usercard"))
                                # 该粉丝ID
                                print('%s  %s' % (user_follower_id[0], user_id))
                                try:
                                    # 输出到数据库的FollowingRelation表
                                    Sqlin_FollowingRelation = MSSQL()
                                    Sqlin_FollowingRelation.iRela(
                                        user_follower_id[0], user_id)
                                except:
                                    print("sql error002")
                        # 找到下一页标签并且点击翻页
                        time.sleep(0.1)
                        # 未知原因，无法翻页，使用当前网址的cookie输入url实现翻页
                        '''
                        next_page = self.driver.find_element_by_xpath(
                            "//a[@class='page next S_txt1 S_line1']")
                        time.sleep(0.1)
                        print('page%s'%i)
                        next_page.click()
                        '''
                        url = 'http://weibo.com/p/100505' + user_id + \
                            '/follow?relate=fans&page=' + \
                            str(i) + '#Pl_Official_HisRelation__60'
                        self.driver.get(url)
                        time.sleep(0.5)
                        # 滚动页面，等待网页刷新，再滚动
                        js = "var q=document .body .scrollTop =10000"
                        self.driver.execute_script(js)
                        time.sleep(0.5)
                    except:
                        print('翻页失败')
                # next page Xpath  //a[@class='page next S_txt1 S_line1']/span
        except:
                print("ERROR:Crawler.getuserfollowers had trouble")

    def getcommentuser(self, user_id):
        try:
            # 从user_id的用户主页微博中的微博评论中get某用户的微博关键词和id
            # 打开用户主页
            mainurl = 'http://weibo.com/' + user_id
            self.driver.set_page_load_timeout(10)
            # self.driver.get(url)
            try:
                self.driver.get(mainurl)
                # ui.WebDriverWait(self.driver, 5).until(self.driver.find_element_by_xpath(
                #    "//span[@class='pt_title S_txt2']"))
            except:
                self.driver.refresh()
            finally:
                # 滚动页面，等待网页刷新，再滚动
                for roll in range(0, 5):
                    js = "var q=document .body .scrollTop =10000"
                    self.driver.execute_script(js)
                    time.sleep(0.5)
                # 当前用户ID
                print('当前用户%s' % user_id)
                # xpath从微博时间中找到该微博的网址
                weibo_xpath = self.driver.find_elements_by_xpath(
                    "//div[@class='WB_detail']/div[2]/a[1]")
                weibo_url = []
                # 将微博链接存入列表
                for p in range(0, len(weibo_xpath)):
                    weibo_url.append(weibo_xpath[p].get_attribute("href"))
                    # print(weibo_url[p])
                # 依次访问微博主页
                for p in range(0, len(weibo_xpath)):

                    self.driver.get(weibo_url[p])
                    # 滚动页面，等待网页刷新，再滚动
                    for roll in range(0, 5):
                        js = "var q=document .body .scrollTop =10000"
                        self.driver.execute_script(js)
                        time.sleep(0.5)
                    # 回复人回复的微博内容与其昵称
                    comments = self.driver.find_elements_by_xpath(
                        "//div[@class='WB_text']")
                    # 得到回复人ID
                    comments_users = self.driver.find_elements_by_xpath(
                        "//div[@class='WB_text']/a[1]")
                    for c in range(0, len(comments)):
                        try:
                            # 正则得到用户ID
                            follower_id = re.findall(
                                r'id=(.*)', comments_users[c].get_attribute('usercard'))
                            # 输出格式ID+昵称微博内容
                            comment = re.findall(r'[：:](.*)', comments[c].text)
                            print('%s %s' % (follower_id[0], comment[0]))
                            fout = open('comment.txt', 'a+', encoding='utf-8')
                            fout.write('%s %s\n' % (follower_id[0], comment[0]))
                            fout.close()
                            try:
                                Sqlin_FollowingRelation = MSSQL()
                                Sqlin_FollowingRelation.iRela(
                                    follower_id[0], user_id)
                            except:
                                print("sql error003")
                        except:
                            # 此人回复的可能是图片，无法输出内容
                            print(follower_id[0])
        except:
                print("ERROR:Crawler.getcommentuser had trouble")


    def getuserweibo(self,user_id):
        #爬取该用户最近的微博（正文）并且做关键词提取
        # 打开用户主页
        mainurl = 'http://weibo.com/' + user_id
        self.driver.set_page_load_timeout(10)
        # self.driver.get(url)
        try:
            self.driver.get(mainurl)
            # ui.WebDriverWait(self.driver, 5).until(self.driver.find_element_by_xpath(
            #    "//span[@class='pt_title S_txt2']"))
        except:
            self.driver.refresh()
        finally:
            # 滚动页面，等待网页刷新，再滚动
            for roll in range(0, 3):
                js = "var q=document .body .scrollTop =10000"
                self.driver.execute_script(js)
                time.sleep(0.4)
            # 当前用户ID
            print('当前用户%s' % user_id)
            # xpath从微博时间中找到该微博的网址
            weibo_xpath = self.driver.find_elements_by_xpath(
                "//div[@class='WB_text W_f14']")

            str=''
            for i in range(0,len(weibo_xpath)):
                str+=weibo_xpath[i].text
            keywords = JA.textrank(str)
            time.sleep(0.01)
            Sqlin = MSSQL()
            for keyword in keywords:
                fout= open('word.txt','a',encoding='utf-8')
                if keyword in WordSet:
                    try:
                        Sqlin.Iuser_word(user_id,keyword)
                        print(keyword)
                    except:
                        print("sqlerr")



