import pymssql
import time


class MSSQL:
    def __getcon(self):
        self.conn = pymssql.connect(host=r"125.216.246.231", user=r"sa",
                                    password=r"3514", database=r"sedb", charset='utf8', as_dict=True)
        cur = self.conn.cursor()
        if not cur:
            raise(NameError, "connecting failed")
        else:
            # print('success connect')
            return cur

    # def ctable(uid)
        # sql="CREATE TABLE "+"U"+uid+"(time datetime,tweet nvarchar(120));"
        # cur=self.__getcon()
        # self.execute(sql)
        # self.conn.close()
        # self.conn.commit()

    def iOuser(self, Oid, ONickName, Gender, Location, Birthday,
               SexualOrientation, Tag, EducationBackground,
               EmotionalSituation, TweetNum, FollowingNum, fansnum):
        sql = "INSERT INTO OrdinaryUserInfo(Oid,ONickName,Gender,Location,Birthday,SexualOrientation,Tag,EducationBackground,EmotionalSituation,TweetNum,FollowingNum,FansNum,dRecordCreationDate) VALUES(\'"
        sql += Oid + "\',\'"
        sql += ONickName + "\',\'"
        sql += Gender + "\',\'"
        sql += Location + "\',\'"
        sql += Birthday + "\',\'"
        sql += SexualOrientation + "\',\'"
        sql += Tag + "\',\'"
        sql += EducationBackground + "\',\'"
        sql += EmotionalSituation + "\',\'"
        sql += TweetNum + "\',\'"
        sql += FollowingNum + "\',\'"
        sql += fansnum + "\',\'"
        sql += time.strftime("%Y-%m-%d ", time.localtime()) + "\')"
        cur = self.__getcon()
        print(sql)
        cur.execute(sql)
        self.conn.commit()
        self.conn.close()

    def iRela(self, oid, bid):
        sql = "INSERT INTO FollowingRelation values(\'"
        sql += oid + "\',\'"
        sql += bid + "\')"
        print(sql)
        cur = self.__getcon()
        cur.execute(sql)
        self.conn.commit()
        self.conn.close()

    def iBuser(self, bid, bnickname, gender, location, birthday,
               tweetnum, followingnum, fansnum, tag, drecord):
        sql = "INSERT INTO BigVInfo(\'"
        sql += bid + "\',\'"
        sql += bnickname + "\',\'"
        sql += gender + "\',\'"
        sql += location + "/',/'"
        sql += birthday + "\',\'"
        sql += tweetnum + "\',\'"
        sql += followingnum + "\',\'"
        sql += fansnum + "\',\'"
        sql += tag + "\')"
        sql += "getdate())"
        cur = self.__getcon()
        cur.execute(sql)
        self.conn.commit()
        self.conn.close()

    def RRela(self):
        sql = "select distinct Oid from FollowingRelation where Oid not in(select Oid from OrdinaryUserInfo)"
        print(sql)
        cur = self.__getcon()
        cur.execute(sql)
        data = cur.fetchall()
        self.conn.commit()
        self.conn.close()
        if data:
            return data
        else:
            print("No data.")

    def Delid(self, oid):
        # 用来删除企业ID
        sql = "delete from FollowingRelation where Oid=\'"
        sql += oid + "\'"
        print(sql)
        cur = self.__getcon()
        cur.execute(sql)
        self.conn.commit()
        self.conn.close()

    def Rwordid(self):
        # 用来从OrdinaryUserInfo表中获得还未进行关键词提取的id
        sql = "select distinct Oid from OrdinaryUserInfo where Oid not in (select Oid from OrdinaryUserKeyword)"
        print(sql)
        cur = self.__getcon()
        cur.execute(sql)
        data = cur.fetchall()
        self.conn.commit()
        self.conn.close()
        if data:
            return data
        else:
            print("No data.")

    def Iuser_word(self, userid, word):
        # 插入该用户的关键字
        sql = "insert into OrdinaryUserKeyword(Oid,Keyword) values (\'"
        sql += userid + "\',\'"
        sql += word + "\')"
        print(sql)
        cur = self.__getcon()
        cur.execute(sql)
        self.conn.commit()
        self.conn.close()

    def dataoidget(self):
        # 返回结果为有标签的用户ID
        sql = "select Oid from OrdinaryUserInfo where [select]<>\'NULL\'"
        print(sql)
        cur = self.__getcon()
        cur.execute(sql)
        data = cur.fetchall()
        self.conn.commit()
        self.conn.close()
        if data:
            return data
        else:
            print('Error or no data!')

    def orgdataget(self, oid):
        # 通过输入的oid来查询该用户的全部信息
        sql = "select Oid,ONickName,Gender,Location,Birthday,SexualOrientation,Tag,EducationBackground,[select] from OrdinaryUserInfo where Oid=\'"
        sql += oid + "\'"
        print(sql)
        cur = self.__getcon()
        cur.execute(sql)
        data = cur.fetchall()
        self.conn.commit()
        self.conn.close()
        if data:
            return data
        else:
            print('Error or no data!')

    def keywordget(self, oid):
        sql = "select Keyword from OrdinaryUserKeyword where Oid=\'"
        sql += oid + "\'"
        print(sql)
        cur = self.__getcon()
        cur.execute(sql)
        data = cur.fetchall()
        self.conn.commit()
        self.conn.close()
        if data:
            return data
        else:
            print('Error or no data!')
