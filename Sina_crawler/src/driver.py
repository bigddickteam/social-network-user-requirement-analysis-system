import time
import re
import os

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.action_chains import ActionChains
class Driver():
    driver = webdriver.Chrome()
    driver.maximize_window()
    wait = ui.WebDriverWait(driver,10)