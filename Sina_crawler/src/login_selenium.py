import time
import re
import os

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.action_chains import ActionChains

from driver import Driver

class Login(Driver):
    driver=Driver.driver
    def login(self, username, password):
        try:
            # 微博登陆网址
            self.driver.get("https://login.sina.com.cn/")
            name = self.driver.find_element_by_name("username")
            name.send_keys(username)  # 用户名
            pwd = self.driver.find_element_by_name("password")
            pwd.send_keys(password)  # 密码
            # 暂停2S，看看网站是否完好，其实如果有验证码的话就需要手动将这个数增大然后手动输入验证码
            time.sleep(2)

            # 在密码对话框输入回车
            pwd.send_keys(Keys.RETURN)
            # 暂停2S等待页面
            time.sleep(2)
            # 输出的Cookies信息，正在考虑是否将Cookie以文件形式保存然后就可以多用户相互切换登陆
            #for cookie in self.driver.get_cookies():
                # print cookie
                #for key in cookie:
                    #print("%s%s" % (key, cookie[key]))

        except:
            print("ERROR:Login had trouble!")

    def loginmm(self, username, password):
        print('使用%s登陆'%username)
        self.driver.get('https://login.sina.com.cn/')
